ALTER SEQUENCE chien_id_seq RESTART WITH 1;

DELETE FROM chien;

--
-- Contenu de la table chien
--

INSERT INTO chien (id,num_puce, nom, couleur, dateNaissance,image) VALUES((select nextval('chien_id_seq')),2243, 'Milou', 'Blanc','12-03-2018','img/fox-terrier.jpg');
INSERT INTO chien (id,num_puce, nom, couleur, dateNaissance,image) VALUES((select nextval('chien_id_seq')),2456, 'Medor', 'Brun mixé','12-03-2013','img/berger-allemand.jpg');
INSERT INTO chien (id,num_puce, nom, couleur, dateNaissance,image) VALUES((select nextval('chien_id_seq')),2987, 'Roxy', 'Brun/Marron','12-03-2016','img/kelpie.jpg');
INSERT INTO chien (id,num_puce, nom, couleur, dateNaissance,image) VALUES((select nextval('chien_id_seq')),2754, 'Rex', 'Brun/Marron foncé','12-03-2017','img/berger-belge-tervueren.jpg');
INSERT INTO chien (id,num_puce, nom, couleur, dateNaissance,image) VALUES((select nextval('chien_id_seq')),2954, 'Billy', 'Sable/Blanc','12-03-2015','img/dogue-de-majorque.jpg');
INSERT INTO chien (id,num_puce, nom, couleur, dateNaissance,image) VALUES((select nextval('chien_id_seq')),2674, 'Luna', 'Blanc/Gris','12-03-2019','img/husky-siberien.jpg');
INSERT INTO chien (id,num_puce, nom, couleur, dateNaissance,image) VALUES((select nextval('chien_id_seq')),2678, 'Rocky', 'Brun/Orange/Blanc','12-03-2014','img/chihuahua.jpg');


--
-- Contenu de la table client
--


ALTER SEQUENCE client_id_seq RESTART WITH 1;
delete from client;
INSERT INTO client (id, date_naissance, email, nom, "password", prenom) VALUES((select nextval('client_id_seq')), '12-10-2000', 'tintin@gmail.com', 'Dupont', md5('tintin59'), 'Tintin');
