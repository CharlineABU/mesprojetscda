<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="org.apache.struts.action.ActionErrors"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<!DOCTYPE html>
<html:html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
<title>Accueil CHIENS_GALAXIE</title>
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="css/mdb.min.css" rel="stylesheet">
<!-- Your custom styles (optional) -->
<link href="css/style.min.css" rel="stylesheet">
<style type="text/css">
html, body, header, .carousel {
	height: 60vh;
}

@media ( max-width : 740px) {
	html, body, header, .carousel {
		height: 100vh;
	}
}

@media ( min-width : 800px) and (max-width: 850px) {
	html, body, header, .carousel {
		height: 100vh;
	}
}
</style>
</head>
<body>

	<!-- Navbar -->
	<nav
		class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
		<div class="container">
			<!-- Brand -->
			<a class="navbar-brand waves-effect" href="lister.do"> <strong
				class="blue-text"></strong>
			</a>

			<!-- Collapse -->
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<!-- Links -->
			<div class="collapse navbar-collapse" id="navbarSupportedContent">

				<!-- Left -->
				<ul class="navbar-nav mr-auto">
					
				</ul>

				<!-- Right -->
				<ul class="navbar-nav nav-flex-icons">

				</ul>
			</div>

		</div>
	</nav>
	<!-- Navbar -->

	<!--Carousel Wrapper-->
	<div id="carousel-example-1z" class="carousel slide carousel-fade pt-4"
		data-ride="carousel">

		<!--Indicators-->
		<ol class="carousel-indicators">
			<li data-target="#carousel-example-1z" data-slide-to="0"
				class="active"></li>
			<li data-target="#carousel-example-1z" data-slide-to="1"></li>
			<li data-target="#carousel-example-1z" data-slide-to="2"></li>
		</ol>
		<!--/.Indicators-->

		<!--Slides-->
		<div class="carousel-inner" role="listbox">

			<!--First slide-->
			<div class="carousel-item active">
				<div class="view"
					style="background-image: url('img/initbdd.png'); background-repeat: no-repeat; background-size: cover;">
					<!-- Mask & flexbox options-->
					<div
						class="mask rgba-black-strong d-flex justify-content-center align-items-center">
						<!-- Content -->
						<div class="text-center white-text mx-5 wow fadeIn">
							<h1 class="mb-4">
								<strong>Bienvenue chez CHIENS-GALAXIE</strong>
							</h1>

							<p>
								<strong>La Galaxie des chiens les plus doux</strong>
							</p>

							<p class="mb-4 d-none d-md-block">
								<strong>Un univers dédié aux chiens pour découvrir
									leurs histoires grâce aux actualités.<br> Des astuces et
									conseils pratiques vous sont également prodigués sur
									l’adoption, l’alimentation, l’éducation pour bien vous occuper
									de votre chien. <br> Vous pourrez aussi découvrir
									l’identité de chaque chien grâce à nos fiches races détaillées.
								</strong>
							</p>
							<br>
							<a href="connexion.do" class="btn btn-outline-white btn-lg">CONNEXION</a>
							<a href="inscription.do" class="btn btn-outline-white btn-lg">INSCRIPTION</a>
						</div>
						<!-- Content -->
					</div>
					<!-- Mask & flexbox options-->

				</div>
			</div>
			<!--/First slide-->
		</div>
		<!--/.Slides-->


		<!--Controls-->
		<a class="carousel-control-prev" href="#carousel-example-1z"
			role="button" data-slide="prev"> <span
			class="carousel-control-prev-icon" aria-hidden="true"></span> <span
			class="sr-only">Previous</span>
		</a> <a class="carousel-control-next" href="#carousel-example-1z"
			role="button" data-slide="next"> <span
			class="carousel-control-next-icon" aria-hidden="true"></span> <span
			class="sr-only">Next</span>
		</a>
		<!--/.Controls-->

	</div>
	<!--Carousel Wrapper-->
	<!--Main layout-->
	<main></main>
	<!--Main layout-->

	<!--Footer-->
	<jsp:include page="footer.jsp"></jsp:include>
	<!--/.Footer-->

	<!-- SCRIPTS -->
	<!-- JQuery -->
	<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
	<!-- Bootstrap tooltips -->
	<script type="text/javascript" src="js/popper.min.js"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<!-- MDB core JavaScript -->
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<!-- Initializations -->
	<script type="text/javascript">
		// Animations initialization
		new WOW().init();
	</script>

</body>
</html:html>