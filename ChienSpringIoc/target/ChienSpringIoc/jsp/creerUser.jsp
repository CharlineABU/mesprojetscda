<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="org.apache.struts.action.ActionErrors"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html:html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
<title>Enregister Nouveau Client</title>
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="css/mdb.min.css" rel="stylesheet">
<!-- Your custom styles (optional) -->
<link href="css/style.min.css" rel="stylesheet">
</head>
<body class="grey lighten-3">
	<!-- Navbar -->
	<nav
		class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
		<div class="container">
			<!-- Brand -->
			<a class="navbar-brand waves-effect" href="lister.do"> <strong
				class="blue-text"></strong>
			</a>

			<!-- Collapse -->
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<!-- Links -->
			<div class="collapse navbar-collapse" id="navbarSupportedContent">

				<!-- Left -->
				<ul class="navbar-nav mr-auto">
					
				</ul>

				<!-- Right -->
				<ul class="navbar-nav nav-flex-icons">

				</ul>
			</div>

		</div>
	</nav>
	<!-- Navbar -->

	<!--Carousel Wrapper-->
	<jsp:include page="header2.jsp"></jsp:include>
	<!--Carousel Wrapper-->
	<!--Main layout-->
	<main class="mt-5 pt-4">
		<div class="container wow fadeIn">

			<!-- Heading -->
			<h2 class="my-5 h2 text-center">Créer Compte User</h2>

			<!--Grid row-->
			<div class="row" style="width: 1650px; margin: 0 auto;">

				<!--Grid column-->
				<div class="col-md-8 mb-4">

					<!--Card-->
					<div class="card">

						<!--Card content-->
						<html:form method="POST" action="/inscrire.do" focus="num_puce"
							styleClass="card-body">

							<!--Grid row-->
							<div class="row">

							</div>
							<!--Grid row-->

							<!--nom-->
							<div class="md-form mb-5">
								<html:text property="nom" styleClass="form-control" />
								<html:errors property="nom" header="errors.field.header"
									footer="errors.field.footer" />
								<label for="nom" class="">Nom</label>
							</div>

							<!--prenom-->
							<div class="md-form mb-5">
								<html:text property="prenom" styleClass="form-control" />
								<html:errors property="prenom" header="errors.field.header"
									footer="errors.field.footer" />
								<label for="prenom" class="">Prénom</label>
							</div>

							<!--email-->
							<div class="md-form mb-5">
								<html:text property="email" styleClass="form-control" />
								<html:errors property="email" header="errors.field.header"
									footer="errors.field.footer" />
								<label for="email" class="">Email</label>
							</div>
							<!--password-->
							<div class="md-form mb-5">
								<html:password property="password" styleClass="form-control" />
								<html:errors property="password" header="errors.field.header"
									footer="errors.field.footer" />
								<label for="password" class="">Mot de passe</label>
							</div>

							<!--date_naissance-->
							<div class="md-form mb-5">
								<html:text property="date_naissance" styleClass="form-control" />
								<html:errors property="date_naissance" header="errors.field.header"
									footer="errors.field.footer" />
								<label for="date_naissance" class="">Date Naissance</label>
							</div>

							<hr class="mb-4">
							<html:submit value="Enregistrer" styleClass="btn btn-primary btn-lg btn-block" />
							<html:link href="accueil.do" >
								<html:button property="accueil" value="RETOUR ACCUEIL" styleClass="btn btn-primary btn-lg btn-block"></html:button>
							</html:link>
						</html:form>
						<%-- Permet d'afficher les messages si present --%>
						<logic:messagesPresent message="true">
							<html:messages id="creationOK" property="creationOK" message="true" header="valid.global.header" footer="valid.global.footer">
								<bean:write name="creationOK" />
							</html:messages>
						</logic:messagesPresent>

						<%-- permet d'afficher les erreurs "globales" --%>
						<html:errors header="errors.global.header" property="creationKO" />
						<br>
					</div>
					<!--/.Card-->

				</div>
				<!--Grid column-->

			</div>
			<!--Grid row-->

		</div>
	</main>
	<!--Main layout-->

	<!--Footer-->
	<jsp:include page="footer.jsp"></jsp:include>
	<!--/.Footer-->

	<!-- SCRIPTS -->
	<!-- JQuery -->
	<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
	<!-- Bootstrap tooltips -->
	<script type="text/javascript" src="js/popper.min.js"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<!-- MDB core JavaScript -->
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<!-- Initializations -->
	<script type="text/javascript">
		// Animations initialization
		new WOW().init();
	</script>

</body>
</html:html>