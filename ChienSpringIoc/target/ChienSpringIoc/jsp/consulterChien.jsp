<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="org.apache.struts.action.ActionErrors"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html:html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
<title>Détail d'un Chien</title>
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="css/mdb.min.css" rel="stylesheet">
<!-- Your custom styles (optional) -->
<link href="css/style.min.css" rel="stylesheet">
</head>
<body>

	<!-- Navbar -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- Navbar -->

	<!--Main layout-->
	<main class="mt-5 pt-4">
		<div class="container dark-grey-text mt-5">
			<c:if test="${chien == null }">
				<p>
					<strong>"LE CHIEN N'EST PAS DISPONIBLE !"</strong>
				</p>
			</c:if>
			<c:if test="${chien != null }">
			<!--Grid row-->
			<div class="row wow fadeIn">

				<!--Grid column-->
				<div class="col-md-6 mb-4">
					<img src="${chien.image }" class="img-fluid" alt="">
				</div>
				<!--Grid column-->

				<!--Grid column-->
				<div class="col-md-6 mb-4">

					<!--Content-->
					<div class="p-4">
						<div class="mb-3">
							<span class="badge purple mr-1">Puce n°${chien.numPuce}</span> 
							<span class="badge blue mr-1">Couleur ${chien.couleur}</span>
							<span class="badge red mr-1">Agé de ${chien.ageChien} an(s)</span>
						</div>

						<p class="lead">
							<span>${chien.nom}</span>
						</p>

						<p class="lead font-weight-bold">Description</p>

						<p>${chien.nom} est un chien de taille moyenne, à l’allure robuste, de couleur sable, chocolat ou noir.
						Très apprécié des familles, il fait souvent le plaisir des enfants avec qui il est un formidable partenaire de jeu.
						Son expression joviale et amicale participe grandement à son succès. 
						${chien.nom} est l’un des chiens préférés des plus jeunes.</p>

					</div>
					<html:link href="modifier_chien.do?id=${chien.id }" styleClass="grey-text"><h5>Update</h5></html:link>
					<!--Content-->
				</div>
				<!--Grid column-->

			</div>
			<!--Grid row-->
			</c:if>
			<hr>

			<!--Grid row-->
			<div class="row d-flex justify-content-center wow fadeIn">

				<!--Grid column-->
				<div class="col-md-6 text-center">

					<h4 class="my-4 h4">Additional information</h4>

					<p>${chien.nom} Retriever est un chien anglais dont les ancêtres ont été ramenés de Terre-Neuve dès 1814.
					L’ancêtre de ${chien.nom} serait le « Chien de St John », une variante plus petite que le Terre-Neuve qui s'est développée au Canada.
					Le chien de Saint-John aidait les marins-pêcheurs pour partir à la recherche de poissons. 
					C’est pour cela que ${chien.nom} mérite l'appellation de Retriever. Il rapporte tout.
					En 1814, il aurait ainsi conquis l’Angleterre.
					La sélection aurait débuté en 1822 où ce chien de Terre-Neuve reçoit le nom de Labrador par le comte de Malmesbury.
					Il débarque en France à la toute fin du 19e siècle. Depuis, il est l’un des chiens préférés des familles françaises.

				</div>
				<!--Grid column-->

			</div>
			<!--Grid row-->

			<!--Grid row-->
			<div class="row wow fadeIn">

				<!--Grid column-->
				<div class="col-lg-4 col-md-12 mb-4">

					<img src="img/dogue-allemand.jpg" class="img-fluid" alt="">

				</div>
				<!--Grid column-->

				<!--Grid column-->
				<div class="col-lg-4 col-md-6 mb-4">

					<img src="img/chihuahua.jpg" class="img-fluid" alt="">

				</div>
				<!--Grid column-->

				<!--Grid column-->
				<div class="col-lg-4 col-md-6 mb-4">

					<img src="img/kelpie.jpg" class="img-fluid" alt="">

				</div>
				<!--Grid column-->

			</div>
			<!--Grid row-->

		</div>
	</main>
	<!--Main layout-->


	<!--Footer-->
	<jsp:include page="footer.jsp"></jsp:include>
	<!--/.Footer-->

	<!-- SCRIPTS -->
	<!-- JQuery -->
	<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
	<!-- Bootstrap tooltips -->
	<script type="text/javascript" src="js/popper.min.js"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<!-- MDB core JavaScript -->
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<!-- Initializations -->
	<script type="text/javascript">
		// Animations initialization
		new WOW().init();
	</script>

</body>
</html:html>