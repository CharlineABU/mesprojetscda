/**
 * 
 */
package presentation.form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author pc
 *
 */
public class ConnexionForm extends ActionForm {

    /**
     * 
     */
    private static final long serialVersionUID = 4775207072211859710L;

    private String            login;
    private String            password;

    @Override
    public void reset(final ActionMapping mapping, final HttpServletRequest request) {
        // TODO Auto-generated method stub
        super.reset(mapping, request);
    }

    @Override
    public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();

        return errors;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

}
