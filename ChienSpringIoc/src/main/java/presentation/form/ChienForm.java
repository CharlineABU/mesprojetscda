/**
 * 
 */
package presentation.form;

import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 * @author pc
 *
 */
public class ChienForm extends ActionForm {

    private String            id;
    private String            numPuce;
    private String            nom;
    private String            couleur;
    private String            ageChien;
    private String            dateNaissance;
    private String            image;

    /**
     * 
     */
    private static final long serialVersionUID = 6438457049925098025L;

    /**
     * Construteur
     */
    public ChienForm() {
        super();
    }

    /**
     * Permet de savoir si un mot est composé de chiffres
     * 
     * @param mot
     * @return true si le mot ne contient que des chiffres, false sinon
     */
    private boolean isInteger(final String mot) {
        // ça c'est pour utiliser les Regex, c'est super fort
        return mot.matches("\\d+");
    }

    @Override
    public void reset(final ActionMapping mapping, final HttpServletRequest request) {
        // TODO Auto-generated method stub
        super.reset(mapping, request);
    }

    @Override
    public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();

        //num_puce
        if (numPuce.isEmpty()) {
            errors.add("numPuce", new ActionMessage("errors.num_puce.obligatoire"));
        } else {
            if (!isInteger(numPuce)) {
                errors.add("numPuce", new ActionMessage("errors.num_puce.mumerique"));
            }
        }

        //Nom
        if (nom.isEmpty()) {
            errors.add("nom", new ActionMessage("errors.nom.obligatoire"));
        }

        //couleur
        if (couleur.isEmpty()) {
            errors.add("couleur", new ActionMessage("errors.couleur.obligatoire"));
        }

        //dateNaissance
        if (dateNaissance.isEmpty()) {
            errors.add("dateNaissance", new ActionMessage("errors.age_chien.obligatoire"));
        } else {
            try {
                new SimpleDateFormat("dd-MM-yyyy").parse(dateNaissance);
            } catch (Exception e) {
                errors.add("dateNaissance", new ActionMessage("errors.date_naissance.formatInvalide"));
            }
        }

        //lien_image
        if (image.isEmpty()) {
            errors.add("image", new ActionMessage("errors.lien_image.obligatoire"));
        }

        return errors;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getNumPuce() {
        return numPuce;
    }

    public void setNumPuce(final String numPuce) {
        this.numPuce = numPuce;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(final String nom) {
        this.nom = nom;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(final String couleur) {
        this.couleur = couleur;
    }

    public String getAgeChien() {
        return ageChien;
    }

    public void setAgeChien(final String ageChien) {
        this.ageChien = ageChien;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(final String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getImage() {
        return image;
    }

    public void setImage(final String image) {
        this.image = image;
    }

}
