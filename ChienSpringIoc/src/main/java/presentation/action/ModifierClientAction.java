/**
 * 
 */
package presentation.action;

import java.text.DateFormat;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.springframework.beans.factory.annotation.Autowired;

import presentation.form.ClientForm;
import service.IClientService;
import service.bean.ClientDto;
import util.MD5Encryption;

/**
 * @author pc
 *
 */
public class ModifierClientAction extends Action {

    @Autowired
    IClientService clientService;
    @Autowired
    DateFormat     dateFormat;

    /**
     * 
     */
    public ModifierClientAction() {
        // TODO Auto-generated constructor stub
    }

    /**
     * Permet de mapper un Clienform en clientDto
     * 
     * @param chienForm
     * @return chienDto
     * @throws ParseException
     */
    public ClientDto mapClientFormToClientDto(final ClientForm clientForm) throws ParseException {

        final ClientDto clientDto = new ClientDto();
        clientDto.setNom(clientForm.getNom());
        clientDto.setPrenom(clientForm.getPrenom());
        clientDto.setDateNaissance(dateFormat.parse(clientForm.getDate_naissance()));
        clientDto.setEmail(clientForm.getEmail());
        clientDto.setPassword(MD5Encryption.cryptage(clientForm.getPassword()));

        return clientDto;

    }

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ClientForm clientForm = (ClientForm) form;
        ClientDto user = (ClientDto) request.getSession().getAttribute("user");
        ClientDto newUser = mapClientFormToClientDto(clientForm);
        newUser.setId(user.getId());

        user = clientService.updateClientDto(newUser);

        request.getSession().setAttribute("user", user);
        if (user == null) {
            final ActionErrors errors = new ActionErrors();
            errors.add("creerUserKO", new ActionMessage("errors.user.update"));
            saveErrors(request, errors);
        } else {
            final ActionMessages messages = new ActionMessages();
            messages.add("updateUserOK", new ActionMessage("update.user.ok", new Object[] { user.getNom() }));
            saveMessages(request, messages);
        }

        if (user == null) {
            return mapping.findForward("notconnected");
        } else {
            return mapping.findForward("success");
        }
    }

}
