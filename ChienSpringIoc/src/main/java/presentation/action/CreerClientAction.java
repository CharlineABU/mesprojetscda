/**
 * 
 */
package presentation.action;

import java.text.DateFormat;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.springframework.beans.factory.annotation.Autowired;

import presentation.form.ClientForm;
import service.IClientService;
import service.bean.ClientDto;
import util.MD5Encryption;

/**
 * @author pc
 *
 */
public class CreerClientAction extends Action {

    @Autowired
    IClientService clientService;
    @Autowired
    DateFormat     dateFormat;

    /**
     * Permet de mapper un Clienform en clientDto
     * 
     * @param chienForm
     * @return chienDto
     * @throws ParseException
     */
    public ClientDto mapClientFormToClientDto(final ClientForm clientForm) throws ParseException {

        final ClientDto clientDto = new ClientDto();
        clientDto.setNom(clientForm.getNom());
        clientDto.setPrenom(clientForm.getPrenom());
        clientDto.setDateNaissance(dateFormat.parse(clientForm.getDate_naissance()));
        clientDto.setEmail(clientForm.getEmail());
        clientDto.setPassword(MD5Encryption.cryptage(clientForm.getPassword()));

        return clientDto;

    }

    @Override
    public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
        // recupere le formulaire chien
        final ClientForm clientForm = (ClientForm) form;

        ClientDto clientDto = mapClientFormToClientDto(clientForm);

        // recupere les info du clientform et on creer le client en BDD
        final ClientDto newClientDto = clientService.createClientDto(clientDto);
        request.getSession().setAttribute("user", newClientDto);
        if (newClientDto == null) {
            final ActionErrors errors = new ActionErrors();
            errors.add("creationKO", new ActionMessage("errors.creation"));
            saveErrors(request, errors);
        } else {
            
            final ActionMessages messages = new ActionMessages();
            messages.add("creationOK", new ActionMessage("creer.ok", new Object[] { newClientDto.getNom() }));
            saveMessages(request, messages);
        }

        return mapping.findForward("success");
    }

}
