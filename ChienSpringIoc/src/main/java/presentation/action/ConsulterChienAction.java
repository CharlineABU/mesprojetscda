/**
 * 
 */
package presentation.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.beans.factory.annotation.Autowired;

import service.IChienService;
import service.bean.ChienDto;
import service.bean.ClientDto;

/**
 * @author Charline
 *
 */
public class ConsulterChienAction extends Action {

    @Autowired
    IChienService chienService;

    @Override
    public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request, final HttpServletResponse response) throws Exception {

        //récupérer le paramètre id du chien à consulter en request
        final int idChien = Integer.parseInt(request.getParameter("id"));
        //chercher le chien en BDD
        final ChienDto chienDto = chienService.findChienDtoById(idChien);
        //mettre le chien trouvé en request
        request.setAttribute("chien", chienDto);

        ClientDto user = (ClientDto) request.getSession().getAttribute("user");
        if (user == null) {
            return mapping.findForward("notconnected");
        } else {
            return mapping.findForward("success");
        }
    }

}
