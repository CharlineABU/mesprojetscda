/**
 * 
 */
package presentation.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.beans.factory.annotation.Autowired;

import persistence.bean.Client;
import service.IChienService;
import service.bean.ChienDto;
import service.bean.ClientDto;

/**
 * @author Charline
 *
 */
public class VoirModifierChienAction extends Action {

    @Autowired
    IChienService chienService;

    @Override
    public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        // recupere l'id du chien à modifier
        final Integer id = Integer.valueOf(request.getParameter("id"));
        //recupere le chien dans la BDD
        ChienDto chienDto = chienService.findChienDtoById(id);
        //mettre le chien dans la requete
        session.setAttribute("chien", chienDto);
        
        ClientDto user = (ClientDto) request.getSession().getAttribute("user");
        if (user == null) {
            return mapping.findForward("notconnected");
        } else {
            return mapping.findForward("success");
        }
    }

}
