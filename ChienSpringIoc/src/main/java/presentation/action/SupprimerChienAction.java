/**
 * 
 */
package presentation.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.springframework.beans.factory.annotation.Autowired;

import persistence.bean.Client;
import service.IChienService;
import service.bean.ChienDto;
import service.bean.ClientDto;

/**
 * @author pc
 *
 */
public class SupprimerChienAction extends Action {

    @Autowired
    IChienService chienService;

    @Override
    public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request, final HttpServletResponse response) throws Exception {

        // recuperer l'id du chien à modifier
        final Integer id = Integer.valueOf(request.getParameter("id"));

        ChienDto chienDto = chienService.findChienDtoById(id);
        //supprimer le chien de la BDD
        boolean resultat = chienService.delete(id);
        if (resultat) {
            final ActionMessages messages = new ActionMessages();
            messages.add("deleteOK", new ActionMessage("supprimer.ok", new Object[] { chienDto.getNom() }));
            saveMessages(request.getSession(), messages);

        } else {
            final ActionErrors errors = new ActionErrors();
            errors.add("deleteKO", new ActionMessage("errors.suppression", new Object[] { chienDto.getNom() }));
            saveErrors(request, errors);

        }

        ClientDto user = (ClientDto) request.getSession().getAttribute("user");
        if (user == null) {
            return mapping.findForward("notconnected");
        } else {
            return mapping.findForward("success");
        }
    }

}
