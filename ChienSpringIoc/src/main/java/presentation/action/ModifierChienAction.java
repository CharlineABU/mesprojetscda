/**
 * 
 */
package presentation.action;

import java.text.DateFormat;
import java.text.ParseException;
import java.time.LocalDate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.springframework.beans.factory.annotation.Autowired;

import persistence.bean.Client;
import presentation.form.ChienForm;
import service.IChienService;
import service.bean.ChienDto;
import service.bean.ClientDto;
import service.bean.mapper.ChienMapper;

/**
 * @author Charline
 *
 */
public class ModifierChienAction extends Action {

    @Autowired
    IChienService chienService;
    @Autowired
    ChienMapper   chienMapper;
    @Autowired
    DateFormat    dateFormat;

    /**
     * Permet de mapper un Chienform en chienDto
     * 
     * @param chienForm
     * @return chienDto
     */
    public ChienDto mapChienFormToChienDto(final ChienForm chienForm) {
        final ChienDto chienDto = new ChienDto();
        chienDto.setId(Integer.valueOf(chienForm.getId()));
        chienDto.setNumPuce(Integer.valueOf(chienForm.getNumPuce()));
        chienDto.setNom(chienForm.getNom());
        chienDto.setCouleur(chienForm.getCouleur());
        try {
            chienDto.setDateNaissance(dateFormat.parse(chienForm.getDateNaissance()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        chienDto.setAgeChien(chienMapper.calculateAge(chienDto.getDateNaissance(), LocalDate.now()));
        chienDto.setImage(chienForm.getImage());

        return chienDto;

    }

    @Override
    public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        //recuperer le formulaire chien
        final ChienForm chienForm = (ChienForm) form;
        //chercher le chien en BDD
        final ChienDto newChienDto = chienService.updateChienDto(mapChienFormToChienDto(chienForm));
        // on teste le retour du service
        if (newChienDto == null) {
            final ActionErrors errors = new ActionErrors();
            errors.add("error", new ActionMessage("errors.modification", new Object[] { chienForm.getNom() }));
            saveErrors(request, errors);
        } else {
            final ActionMessages messages = new ActionMessages();
            messages.add("updateOK", new ActionMessage("editer.ok", new Object[] { chienForm.getNom() }));
            saveMessages(request, messages);
            session.setAttribute("chien", newChienDto);
        }

        ClientDto user = (ClientDto) request.getSession().getAttribute("user");
        if (user == null) {
            return mapping.findForward("notconnected");
        } else {
            return mapping.findForward("success");
        }
    }

}
