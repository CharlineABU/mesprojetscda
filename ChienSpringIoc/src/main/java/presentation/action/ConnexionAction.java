/**
 * 
 */
package presentation.action;

import java.text.DateFormat;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.springframework.beans.factory.annotation.Autowired;

import presentation.form.ClientForm;
import presentation.form.ConnexionForm;
import service.IClientService;
import service.bean.ClientDto;
import util.MD5Encryption;

/**
 * @author Charline
 *
 */
public class ConnexionAction extends Action {
    
    @Autowired
    IClientService clientService;
    @Autowired
    DateFormat     dateFormat;
    
    
    /**
     * Permet de mapper un Clientform en clientDto
     * 
     * @param chienForm
     * @return chienDto
     * @throws ParseException
     */
    public ClientDto mapClientFormToClientDto(final ClientForm clientForm) throws ParseException {

        final ClientDto clientDto = new ClientDto();
        clientDto.setNom(clientForm.getNom());
        clientDto.setPrenom(clientForm.getPrenom());
        clientDto.setDateNaissance(dateFormat.parse(clientForm.getDate_naissance()));
        clientDto.setEmail(clientForm.getEmail());
        clientDto.setPassword(clientForm.getPassword());

        return clientDto;

    }

    @Override
    public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
      
        ConnexionForm connexionForm = (ConnexionForm) form;
        
        ClientDto user = clientService.rechercherParLoginEtPassword(connexionForm.getLogin(), MD5Encryption.cryptage(connexionForm.getPassword()));
        
        if (user != null) {
            request.getSession().setAttribute("user", user);
            return mapping.findForward("success");
        }else {
            final ActionErrors errors = new ActionErrors();
            errors.add("connexionKO", new ActionMessage("errors.connexion"));
            saveErrors(request, errors);
            return mapping.findForward("echec");
        }

    }

}
