/**
 * 
 */
package persistence.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

/**
 * @author Charline
 *
 */
@Data
@Entity()
@Table(name = "chien")
@NamedQuery(name = "all", query = "SELECT c From ChienDo c")
@SequenceGenerator(name = "chien_id_seq",initialValue = 1,allocationSize = 1)
public class ChienDo {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "chien_id_seq")
    private Integer id;

    @Column(name = "num_puce")
    private Integer numPuce;

    @Column(name = "nom")
    private String  nom;

    @Column(name = "couleur")
    private String  couleur;

    @Temporal(TemporalType.DATE)
    @Column(name = "dateNaissance")
    private Date    dateNaissance;

    @Column(name = "image")
    private String  image;

}
