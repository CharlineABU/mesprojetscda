/**
 * 
 */
package persistence.dao.imp;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import persistence.bean.ChienDo;
import persistence.dao.IChienDao;

/**
 * @author Charline
 *
 */
@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class ChienDao implements IChienDao {

    @PersistenceContext(unitName = "puChien", type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    @Override
    public ChienDo createChienDo(final ChienDo chienDo) {
        entityManager.persist(chienDo);
        //je retourne le produitDo crée
        return chienDo;
    }

    @Override
    public ChienDo findChienDo(final Integer idChienDo) {
        //je retourne le chienDo trouvé
        return entityManager.find(ChienDo.class, idChienDo);
    }

    @Override
    public List<ChienDo> findAllChienDo() {
        TypedQuery<ChienDo> typedQuery = entityManager.createQuery("select c from ChienDo c", ChienDo.class);
        return typedQuery.getResultList();
    }

    @Override
    public ChienDo updateChienDo(final ChienDo chienDo) {
        //je retourne le chienDo màj
        return entityManager.merge(chienDo);
    }

    @Override
    public boolean deleteChienDo(final Integer idChienDo) {
        ChienDo chienDo = findChienDo(idChienDo);
        if (chienDo == null) {
            return false;
        }
        entityManager.remove(chienDo);
        return true;
    }

}
