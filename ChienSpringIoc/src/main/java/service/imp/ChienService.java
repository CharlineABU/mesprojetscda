package service.imp;

import java.text.DateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import persistence.bean.ChienDo;
import persistence.dao.IChienDao;
import service.IChienService;
import service.bean.ChienDto;
import service.bean.mapper.ChienMapper;

/**
 * @author Charline
 *
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class ChienService implements IChienService {

    @Autowired
    IChienDao   chienDao;
    @Autowired
    ChienMapper chienMapper;
    @Autowired
    DateFormat dateFormat;

    /**
     * Permet de tester si le numero de puce existe déjà
     * 
     * @param chienDto
     * @return true si le numero existe sinon false
     */
    private boolean findNumPuce(final ChienDto chienDto) {
        final List<ChienDo> listeChiens = chienDao.findAllChienDo();
        for (int i = 0; i < listeChiens.size(); i++) {
            if (chienDto.getNumPuce().intValue() != listeChiens.get(i).getNumPuce().intValue()) {
            }else {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<ChienDto> findAllChienDto() {
        //acces à  la couche persistance
        final List<ChienDto> listeChienDto = chienMapper.mapListChienDoToListChienDto(chienDao.findAllChienDo());
        if (!listeChienDto.isEmpty()) {
            return listeChienDto;
        }
        return null;
    }

    @Override
    public ChienDto findChienDtoById(final Integer id) {
        final ChienDto chienDto = chienMapper.mapChienDoToChienDto(chienDao.findChienDo(id));
        if (chienDto != null) {
            return chienDto;
        }
        return null;
    }

    @Override
    public ChienDto updateChienDto(final ChienDto newChienDto) {
        boolean testNumPuce =findNumPuce(newChienDto);
        ChienDo chienDo=chienDao.findChienDo(newChienDto.getId());
        if ((newChienDto.getId().intValue() != chienDo.getId().intValue())) {
            //mauvais chien
            return null;
        }else if ((chienDo.getNumPuce().intValue()!=newChienDto.getNumPuce().intValue())) {
            //meme chien mais num puce différent
            if (testNumPuce == true) {
                //nouveau num puce exite
                return null;
            }
        } 
        
        chienDo = chienDao.updateChienDo(chienMapper.mapChienDtoToChienDo(newChienDto));
        return chienMapper.mapChienDoToChienDto(chienDo);

    }

    @Override
    public ChienDto createChienDto(final ChienDto chienDto) {
        final ChienDo chienDo = chienMapper.mapChienDtoToChienDo(chienDto);
        if (findNumPuce(chienDto) == true) {
            //Un chien porte deja ce numéro de puce
            return null;
        }
        final ChienDto newChienDto = chienMapper.mapChienDoToChienDto(chienDao.createChienDo(chienDo));
        return newChienDto;
    }

    @Override
    public boolean delete(final Integer id) {
        final boolean resultat = chienDao.deleteChienDo(id);
        return resultat;
    }

}
