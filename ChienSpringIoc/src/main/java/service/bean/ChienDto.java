/**
 * 
 */
package service.bean;

import java.util.Date;

import lombok.Data;

/**
 * @author pc
 *
 */
@Data
public class ChienDto {

    private Integer id;
    private Integer numPuce;
    private String  nom;
    private String  couleur;
    private Date    dateNaissance;
    private Integer ageChien;
    private String  image;

    /**
     * Constructeur
     */
    public ChienDto() {
        super();
    }

}
