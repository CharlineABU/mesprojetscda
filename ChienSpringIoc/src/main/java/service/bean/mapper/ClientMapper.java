/**
 * 
 */
package service.bean.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import persistence.bean.Client;
import service.bean.ClientDto;

/**
 * @author pc
 *
 */
@Component
public class ClientMapper {

    /**
     * Constructeur
     */
    public ClientMapper() {
        super();
    }

    /**
     * Permet de mapper un ClientDto en Client
     * 
     * @return client
     */
    public Client mapToEntity(final ClientDto clientDto) {
        if (clientDto == null)
            return null;
        final Client client = new Client();
        client.setId(client.getId());
        client.setNom(clientDto.getNom());
        client.setPrenom(clientDto.getPrenom());
        client.setEmail(clientDto.getEmail());
        client.setDateNaissance(clientDto.getDateNaissance());
        client.setPassword(clientDto.getPassword());

        return client;
    }

    /**
     * Permet de mapper un Client en ClientDto
     * 
     * @return clientDto
     */
    public ClientDto mapToDto(final Client client) {

        if (client == null)
            return null;

        final ClientDto clientDto = new ClientDto();
        clientDto.setId(client.getId());
        clientDto.setNom(client.getNom());
        clientDto.setPrenom(client.getPrenom());
        clientDto.setEmail(client.getEmail());
        clientDto.setDateNaissance(client.getDateNaissance());
        clientDto.setPassword(client.getPassword());

        return clientDto;
    }

    /**
     * Permet de mapper une liste de Client en une liste de clientDto
     * 
     * @param listeClient
     * @return listeClientDto
     */
    public List<ClientDto> mapListToDto(final List<Client> listeClient) {
        if (listeClient == null)
            return new ArrayList<ClientDto>();
        final List<ClientDto> listeClientDto = new ArrayList<ClientDto>();

        for (Client client : listeClient) {
            listeClientDto.add(mapToDto(client));
        }

        return listeClientDto;
    }

}
