/**
 * 
 */
package service.bean.mapper;

import java.text.DateFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import persistence.bean.ChienDo;
import service.bean.ChienDto;

/**
 * @author Charline
 *
 */
@Component
public class ChienMapper {

    @Autowired
    DateFormat dateFormat;

    /**
     * Permet de convertir une java.util.Date en java.time.LocalDate
     * 
     * @param dateToConvert
     * @return
     */
    public LocalDate convertToLocalDateViaInstant(final Date dateToConvert) {
        return dateToConvert.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    /**
     * Permet de calcul l'age à partir de la date de naissance et de la date du jour
     * 
     * @param birthDate
     * @param currentDate
     * @return age calculé
     */
    public int calculateAge(final Date birthDate, final LocalDate currentDate) {
        final LocalDate dateNaissance = convertToLocalDateViaInstant(birthDate);

        if ((birthDate != null) && (currentDate != null)) {
            int age = Period.between(dateNaissance, currentDate).getYears();
            return age;
        } else {
            return 0;
        }
    }

    /**
     * Permet de mapper un ChienDto en ChienDo
     * 
     * @return chienDo
     */
    public ChienDo mapChienDtoToChienDo(final ChienDto chienDto) {
        if (chienDto == null) {
            return null;
        }
        final ChienDo chienDo = new ChienDo();
        chienDo.setId(chienDto.getId());
        chienDo.setNom(chienDto.getNom());
        chienDo.setNumPuce(chienDto.getNumPuce());
        chienDo.setCouleur(chienDto.getCouleur());
        chienDo.setDateNaissance(chienDto.getDateNaissance());
        chienDo.setImage(chienDto.getImage());

        return chienDo;
    }

    /**
     * Permet de mapper un ChienDo en ChienDto
     * 
     * @return chienDto
     */
    public ChienDto mapChienDoToChienDto(final ChienDo chienDo) {
        if (chienDo == null) {
            return null;
        }
        final ChienDto chienDto = new ChienDto();
        chienDto.setId(chienDo.getId());
        chienDto.setNom(chienDo.getNom());
        chienDto.setNumPuce(chienDo.getNumPuce());
        chienDto.setCouleur(chienDo.getCouleur());
        chienDto.setDateNaissance(chienDo.getDateNaissance());
        chienDto.setImage(chienDo.getImage());

        String dateNaissance = dateFormat.format(chienDo.getDateNaissance());
        try {
            Date dateNaissan = dateFormat.parse(dateNaissance);
            chienDto.setAgeChien(calculateAge(dateNaissan, LocalDate.now()));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return chienDto;
    }

    /**
     * Permet de mapper une liste de ChienDo en une liste de chienDto
     * 
     * @param listeChienDo
     * @return listeChienDto
     */
    public List<ChienDto> mapListChienDoToListChienDto(final List<ChienDo> listeChienDo) {
        final List<ChienDto> listeChienDto = new ArrayList<ChienDto>();

        for (ChienDo chienDo : listeChienDo) {
            dateFormat.format(chienDo.getDateNaissance());
            listeChienDto.add(mapChienDoToChienDto(chienDo));
        }

        return listeChienDto;
    }

}
