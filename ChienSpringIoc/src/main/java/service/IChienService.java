/**
 * 
 */
package service;

import java.util.List;

import service.bean.ChienDto;

/**
 * @author Charline
 *
 */
public interface IChienService {

    /**
     * Permet de recuperer la liste de tous les chiens
     * 
     * @return listeDesChiens
     */
    List<ChienDto> findAllChienDto();

    /**
     * Permet de chercher un chien à partir de son id
     * 
     * @param id du chien à  chercher
     * @return chien trouvée
     */
    ChienDto findChienDtoById(final Integer id);

    /**
     * Permet de mettre à  jour un chien
     * 
     * @param newChienDto
     * @return chienDto mis à jour
     */
    ChienDto updateChienDto(final ChienDto newChienDto);

    /**
     * Permet d'ajouter un nouveau chien dans la liste des chiens
     * 
     * @param chienDto
     * @return le chien crée
     */
    ChienDto createChienDto(final ChienDto chienDto);

    /**
     * Permet de supprimer un chien
     * 
     * @param id du chien à supprimer
     * @return true si delete OK, sinon false
     */
    boolean delete(final Integer id);

}
