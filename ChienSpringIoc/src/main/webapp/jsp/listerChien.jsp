<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="org.apache.struts.action.ActionErrors"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<!DOCTYPE html>
<html:html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
<title>Liste de Chiens</title>
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="css/mdb.min.css" rel="stylesheet">
<!-- Your custom styles (optional) -->
<link href="css/style.min.css" rel="stylesheet">
<style type="text/css">
html, body, header, .carousel {
	height: 60vh;
}

@media ( max-width : 740px) {
	html, body, header, .carousel {
		height: 100vh;
	}
}

@media ( min-width : 800px) and (max-width: 850px) {
	html, body, header, .carousel {
		height: 100vh;
	}
}
</style>
</head>
<body >
	
	<!-- Navbar -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- Navbar -->
	
	<!--Carousel Wrapper-->
	<jsp:include page="header2.jsp"></jsp:include>
	<!--Carousel Wrapper-->
	
	<!--Main layout-->
	<main>
		<div class="container">
			<!--Navbar-->
			<jsp:include page="navbarListe.jsp"></jsp:include>
			<!--/.Navbar-->

			<!--Section: Products v.3-->
			
			<!-- Permet d'afficher les messages pour la suppression -->
			<logic:messagesPresent message="true">
				<html:messages id="deleteOK" property="deleteOK" message="true"
					header="valid.global.header" footer="valid.global.footer">
					<bean:write name="deleteOK" />
				</html:messages>
			</logic:messagesPresent>
			<!-- permet d'afficher les erreurs "globales" -->
				<html:errors header="errors.global.header" property="deleteKO" />

			<logic:empty name="listeChiens">
				<p>
					<strong>"PAS DE CHIEN DISPONIBLE !"</strong>
				</p>
			</logic:empty>

			<logic:notEmpty name="listeChiens">

					<section class="text-center mb-4">

						<!--Grid row-->
						<div class="row wow fadeIn">

						<logic:iterate id="chien" collection="${listeChiens}">
							<!--Grid column-->
							<div class="col-lg-3 col-md-6 mb-4">

								<!--Card-->
								<div class="card">

									<!--Card image-->
									<div class="view overlay">
										<img src="${chien.image }" class="card-img-top" alt="">
										<html:link href="consulter.do?id=${chien.id }">
											<div class="mask rgba-white-slight"></div>
										</html:link>
									</div>
									<!--Card image-->

									<!--Card content-->
									<div class="card-body text-center">
										<!--Category & Title-->
										<html:link href="modifier_chien.do?id=${chien.id }"
											styleClass="grey-text">
											<h5>Update</h5>
										</html:link>
										<h5>
											<strong> <html:link
													href="consulter.do?id=${chien.id }"
													styleClass="dark-grey-text">${chien.nom}</html:link></strong>
										</h5>
										<h4 class="font-weight-bold blue-text">
											<strong>${chien.ageChien }</strong>
										</h4>
									</div>
									<!--Card content-->

								</div>
								<!--Card-->

							</div>
							<!--Grid column-->
						</logic:iterate>

					</div>
						<!--Grid row-->
					</section>
					<!--Section: Products v.3-->
			</logic:notEmpty>

			<!--Pagination-->
			<jsp:include page="pagination.jsp"></jsp:include>
			<!--Pagination-->

		</div>
	</main>
	<!--Main layout-->

	<!--Footer-->
	<jsp:include page="footer.jsp"></jsp:include>
	<!--/.Footer-->

	<!-- SCRIPTS -->
	<!-- JQuery -->
	<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
	<!-- Bootstrap tooltips -->
	<script type="text/javascript" src="js/popper.min.js"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<!-- MDB core JavaScript -->
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<!-- Initializations -->
	<script type="text/javascript">
		// Animations initialization
		new WOW().init();
	</script>

</body>
</html:html>