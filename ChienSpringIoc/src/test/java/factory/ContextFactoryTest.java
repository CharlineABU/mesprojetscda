/**
 * 
 */
package factory;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import util.ContextConfigurationType;
import util.ContextFactory;

/**
 * @author Charline
 *
 */
class ContextFactoryTest {

    /**
     * Test method for {@link util.ContextFactory#getContext(util.ContextConfigurationType)}.
     */
    @Test
    void testGetContext() {
        assertNotNull(ContextFactory.getContext(ContextConfigurationType.CLASSPATH));
    }

}
