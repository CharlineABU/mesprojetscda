/**
 * 
 */
package service;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.text.DateFormat;
import java.text.ParseException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import service.bean.ChienDto;

/**
 * @author pc
 *
 */
//Permet de gérer le JUnit avec Spring 
@ExtendWith(SpringExtension.class)
//Et de déclarer le fichier de conf à utiliser
@ContextConfiguration("/applicationContext.xml")
class ChienServiceTest {

    @Autowired
    private IChienService iChienService;
    @Autowired
    DateFormat            dateFormat;

    /**
     * Test method for {@link service.imp.ChienService#findAllChienDto()}.
     */
    @Test
    void testFindAllChienDto() {
        assertEquals(7, iChienService.findAllChienDto().size());
    }

    /**
     * Test method for {@link service.imp.ChienService#findChienDtoById(java.lang.Integer)}.
     */
    @Test
    void testFindChienDtoById() {
        assertEquals("Milou", iChienService.findChienDtoById(1).getNom());

    }

    /**
     * Test method for {@link service.imp.ChienService#updateChienDto(service.bean.ChienDto)}.
     */
    @Test
    void testUpdateChienDto() {
        final ChienDto chienDto = iChienService.findChienDtoById(3);
        chienDto.setNom("Luxy JUnit update");
        chienDto.setCouleur("Noir");
        try {
            chienDto.setDateNaissance(dateFormat.parse("25-07-2014"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assertNotNull(iChienService.updateChienDto((chienDto)));
        assertEquals("Luxy JUnit update", iChienService.updateChienDto((chienDto)).getNom());
        assertEquals("Noir", iChienService.updateChienDto(chienDto).getCouleur());
    }

    /**
     * Test method for {@link service.imp.ChienService#createChienDto(service.bean.ChienDto)}.
     */
    @Test
    void testCreateChienDto() {
        final ChienDto chienDto = new ChienDto();
        chienDto.setNumPuce(3421);
        chienDto.setNom("Soso JUnit create");
        chienDto.setCouleur("Bleu");
        try {
            chienDto.setDateNaissance(dateFormat.parse("14-05-2010"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        chienDto.setImage("img/golden-retriever.jpg");
        assertNull(chienDto.getId());
        ChienDto newChien = iChienService.createChienDto(chienDto);
        assertNotNull(newChien);
        assertEquals(8, newChien.getId().intValue());
    }

    /**
     * Test method for {@link service.imp.ChienService#delete(java.lang.Integer)}.
     */
    @Test
    void testDelete() {
        final ChienDto chienDto = iChienService.findChienDtoById(2);
        assertEquals("Medor", chienDto.getNom());
        assertTrue(iChienService.delete(chienDto.getId()));
        assertNull(iChienService.findChienDtoById(2));
    }

}
