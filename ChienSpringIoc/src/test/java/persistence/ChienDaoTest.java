/**
 * 
 */
package persistence;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.transaction.IllegalTransactionStateException;

import persistence.dao.IChienDao;
import util.ContextConfigurationType;
import util.ContextFactory;

/**
 * @author Charline
 *
 */
class ChienDaoTest {

    /**
     * Permet de tester que les transactions sont deleguées à la couche service
     * 
     * Test method for {@link persistence.dao.imp.ChienDao#findAllChienDo()}.
     */
    @Test
    void testFindAllChienDo() {
        final IChienDao iChienDao = ContextFactory.getContext(ContextConfigurationType.CLASSPATH).getBean("chienDao", IChienDao.class);
        // on vérifie qu'on ne peut pas appeler un Dao directement 
        assertThrows(IllegalTransactionStateException.class, () -> {
            iChienDao.findAllChienDo();
        });
    }

}
