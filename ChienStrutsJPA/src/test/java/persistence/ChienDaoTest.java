/**
 * 
 */
package persistence;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import persistence.bean.ChienDo;
import persistence.dao.IChienDao;
import persistence.dao.imp.ChienDao;
import persistence.factory.JpaFactory;

/**
 * @author Charline
 *
 */

class ChienDaoTest {

    @BeforeEach
    public void initData() {
        //je recupère l'entityManagerFactory
        final EntityManagerFactory entityManagerFactory = JpaFactory.getEntityManagerFactory();
        //je crée une entityManager
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        //je crée la transaction
        final EntityTransaction entityTransaction = entityManager.getTransaction();
        //je débute la transaction
        entityTransaction.begin();

        //On lit le fichier
        try (final Scanner sc = new Scanner(new FileReader("src/test/java/dataSet/insert.sql"))) {
            while (sc.hasNext()) {
                final String sql = sc.nextLine();
                //pour chaque ligne non vide
                if (!sql.isEmpty()) {
                    //on l'execute en tant que query native(sql natif)
                    final Query query = entityManager.createNativeQuery(sql);
                    query.executeUpdate();
                }
            }

        } catch (final FileNotFoundException e) {
            e.printStackTrace();
        }
        entityTransaction.commit();
        entityManager.close();
    }

    /**
     * Test method for {@link persistence.dao.imp.ChienDao#createChienDo(persistence.bean.ChienDo)}.
     */
    @Test
    void testCreateChienDo() {
        final IChienDao iChienDao = new ChienDao();
        final ChienDo chienDo = new ChienDo().builder(2222, "Junit Create", "Blanc", 5, "img/labrador-retriever.jpg");

        assertNotNull(iChienDao.createChienDo(chienDo));
        assertNotNull(iChienDao.findChienDo((chienDo.getId())));

    }

    /**
     * Test method for {@link persistence.dao.imp.ChienDao#findChienDo(java.lang.Integer)}.
     */
    @Test
    void testFindChienDo() {
        final IChienDao iChienDao = new ChienDao();
        final ChienDo chienDo = iChienDao.findChienDo(2);

        assertNotNull(chienDo);
        assertEquals(Integer.valueOf(2), chienDo.getId());
        assertEquals("Medor", chienDo.getNom());
        assertNull(iChienDao.findChienDo(0));
    }

    /**
     * Test method for {@link persistence.dao.imp.ChienDao#findAllChienDo()}.
     */
    @Test
    void testFindAllChienDo() {
        final IChienDao iChienDao = new ChienDao();
        final List<ChienDo> listeDesChiens = iChienDao.findAllChienDo();
        assertEquals(7, listeDesChiens.size());
    }

    /**
     * Test method for {@link persistence.dao.imp.ChienDao#updateChienDo(persistence.bean.ChienDo)}.
     */
    @Test
    void testUpdateChienDo() {
        final IChienDao iChienDao = new ChienDao();
        final ChienDo chienDo = iChienDao.findChienDo(6);
        chienDo.setNom("LUNA JUNIT Update");
        iChienDao.updateChienDo(chienDo);
        assertEquals("LUNA JUNIT Update", chienDo.getNom());

    }

    /**
     * Test method for {@link persistence.dao.imp.ChienDao#deleteChienDo(java.lang.Integer)}.
     */
    @Test
    void testDeleteChienDo() {
        final IChienDao iChienDao = new ChienDao();
        //test suppression d'un produit qui n'existe pas
        assertFalse(iChienDao.deleteChienDo(0));
        //test suppression d'un produit 
        assertTrue(iChienDao.deleteChienDo(4));
        //test suppression d'un produit supprimé
        assertNull(iChienDao.findChienDo(4));

    }

}
