package service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import persistence.bean.Client;
import persistence.dao.IClientDao;
import service.IClientService;
import service.dto.ClientDto;
import service.dto.mapper.ClientMapper;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class ClientService implements IClientService {

	@Autowired
	IClientDao clientDao;
	@Autowired
	ClientMapper clientMapper;

	@Override
	public List<ClientDto> findAllClientDto() {
		return clientMapper.mapListToDto(clientDao.findAll());
	}

	@Override
	public ClientDto findClientDtoById(Integer id) {
		return clientMapper.mapToDto(clientDao.findById(id));
	}

	@Override
	public ClientDto updateClientDto(ClientDto newClientDto) {
		ClientDto clientDto = null;

		Client client = clientMapper.mapToEntity(newClientDto);

		client = clientDao.update(client);

		clientDto = clientMapper.mapToDto(client);

		return clientDto;
	}

	@Override
	public ClientDto createClientDto(ClientDto newClientDto) {

		ClientDto clientDto = null;

		Client client = clientMapper.mapToEntity(newClientDto);

		client = clientDao.update(client);

		clientDto = clientMapper.mapToDto(client);

		return clientDto;
	}

	@Override
	public void delete(Integer id) {
		delete(id);
	}

	@Override
	public ClientDto rechercherParLoginEtPassword(String login, String password) {

		Client client = clientDao.findByLoginAndPassword(login, password);

		return clientMapper.mapToDto(client);

	}
	
	@Override
	public Boolean ExistsEmail(String login) {

		Boolean resultat = clientDao.findByEmail(login);

		return resultat;

	}

}
