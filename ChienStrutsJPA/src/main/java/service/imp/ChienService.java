package service.imp;

import java.util.List;

import persistence.bean.ChienDo;
import persistence.dao.imp.ChienDao;
import presentation.bean.ChienDto;
import service.ChienMapper;
import service.IChienService;

/**
 * @author Charline
 *
 */
public class ChienService implements IChienService {

    private static ChienService chienService = null;
    final ChienDao              chienDao     = ChienDao.getInstance();

    /**
     * Constructeur
     */
    public ChienService() {
        super();
    }

    /**
     * Gestion du singleton
     * 
     * @return chienService
     */
    public static ChienService getIntance() {
        if (chienService == null) {
            chienService = new ChienService();
        }
        return chienService;
    }

    /**
     * Permet de tester si le numero de puce existe déjà
     * 
     * @param chienDto
     * @return true si le numero existe sinon false
     */
    private boolean findNumPuce(final ChienDto chienDto) {
        final List<ChienDo> listeChiens = chienDao.findAllChienDo();
        boolean test = false;
        for (final ChienDo chienDo : listeChiens) {
            if (chienDto.getNum_puce() == chienDo.getNum_puce()) {
                test = true;
            }
        }
        return test;
    }

    @Override
    public List<ChienDto> findAllChienDto() {
        //acces à  la couche persistance
        final List<ChienDto> listeChienDto = ChienMapper.mapListChienDoToListChienDto(chienDao.findAllChienDo());
        if (!listeChienDto.isEmpty()) {
            return listeChienDto;
        }
        return null;
    }

    @Override
    public ChienDto findChienDtoById(final Integer id) {
        final ChienDto chienDto = ChienMapper.mapChienDoToChienDto(chienDao.findChienDo(id));
        if (chienDto != null) {
            return chienDto;
        }
        return null;
    }

    @Override
    public ChienDto updateChienDto(final ChienDto newChienDto) {

        if (!findNumPuce(newChienDto) & newChienDto.getId() == chienDao.findChienDo(newChienDto.getId()).getId()) {
            final ChienDo chienDo = chienDao.updateChienDo(ChienMapper.mapChienDtoToChienDo(newChienDto));
            return ChienMapper.mapChienDoToChienDto(chienDo);
        }

        return null;
    }

    @Override
    public ChienDto createChienDto(final ChienDto chienDto) {
        final ChienDo chienDo = ChienMapper.mapChienDtoToChienDo(chienDto);
        if (findNumPuce(chienDto) == true) {
            //Un chien porte deja ce numéro de puce
            return null;
        }
        final ChienDto newChienDto = ChienMapper.mapChienDoToChienDto(chienDao.createChienDo(chienDo));
        return newChienDto;
    }

    @Override
    public boolean delete(final Integer id) {
        final boolean resultat = chienDao.deleteChienDo(id);
        return resultat;
    }

}
