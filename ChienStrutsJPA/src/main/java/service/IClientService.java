/**
 * 
 */
package service;

import java.util.List;

import service.dto.ClientDto;

/**
 * @author Charline
 *
 */
public interface IClientService {
    
    
    /**
     * Permet de recuperer la liste de tous les clients
     * 
     * @return listeDesClients
     */
    List<ClientDto> findAllClientDto() ;

    /**
     * Permet de chercher un client à partir de son id
     * 
     * @param id du client à  chercher
     * @return client trouvée
     */
    ClientDto findClientDtoById(final Integer id) ;
    /**
     * Permet de mettre à  jour un client
     * 
     * @param newClientDto
     * @return clientDto mis à jour
     */
    ClientDto updateClientDto(final ClientDto newClientDto);

    /**
     * Permet d'ajouter un nouveau client dans la liste des clients
     * 
     * @param clientDto
     * @return le client crée
     */
    ClientDto createClientDto(final ClientDto clientDto) ;

    
    /**
     *  Permet de supprimer un client
     *  
     * @param id du client à supprimer
     * @return true si delete OK, sinon false
     */
    void delete(final Integer id) ;
    
    /**
     *  Permet de supprimer un client
     *  
     * @param id du client à supprimer
     * @return true si delete OK, sinon false
     */
    ClientDto rechercherParLoginEtPassword(final String login,String Password) ;

	Boolean ExistsEmail(String login);

}
