/**
 * 
 */
package service.dto;

import java.util.Date;

import lombok.Data;

/**
 * @author Charline
 *
 */
@Data
public class ClientDto {

	private Integer id;

	private String nom;

	private String prenom;

	private String email;

	private String password;

	private Date dateNaissance;

}
