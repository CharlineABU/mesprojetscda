/**
 * 
 */
package service;

import java.util.ArrayList;
import java.util.List;

import persistence.bean.ChienDo;
import presentation.bean.ChienDto;

/**
 * @author pc
 *
 */
public class ChienMapper {

    /**
     * Constructeur
     */
    public ChienMapper() {
        super();
    }

    /**
     * Permet de mapper un ChienDto en ChienDo
     * 
     * @return chienDo
     */
    public static ChienDo mapChienDtoToChienDo(final ChienDto chienDto) {
        final ChienDo chienDo = new ChienDo();
        chienDo.setId(chienDto.getId());
        chienDo.setNom(chienDto.getNom());
        chienDo.setNum_puce(chienDto.getNum_puce());
        chienDo.setCouleur(chienDto.getCouleur());
        chienDo.setAge_chien(chienDto.getAge_chien());
        chienDo.setLien_image(chienDto.getLien_image());

        return chienDo;
    }

    /**
     * Permet de mapper un ChienDo en ChienDto
     * 
     * @return chienDto
     */
    public static ChienDto mapChienDoToChienDto(final ChienDo chienDo) {
        final ChienDto chienDto = new ChienDto();
        chienDto.setId(chienDo.getId());
        chienDto.setNom(chienDo.getNom());
        chienDto.setNum_puce(chienDo.getNum_puce());
        chienDto.setCouleur(chienDo.getCouleur());
        chienDto.setAge_chien(chienDo.getAge_chien());
        chienDto.setLien_image(chienDo.getLien_image());

        return chienDto;
    }

    /**
     * Permet de mapper une liste de ChienDo en une liste de chienDto
     * 
     * @param listeChienDo
     * @return listeChienDto
     */
    public static List<ChienDto> mapListChienDoToListChienDto(final List<ChienDo> listeChienDo) {
        final List<ChienDto> listeChienDto = new ArrayList<ChienDto>();

        for (ChienDo chienDo : listeChienDo) {
            listeChienDto.add(mapChienDoToChienDto(chienDo));
        }

        return listeChienDto;
    }

}
