package persistence.dao;

import java.util.List;

import persistence.bean.ChienDo;

public interface IChienDao {

    /**
     * Permet de créer un chien en BD
     * 
     * @param chienDo le chien à créer en BD
     * @return le chien créé, avec son "Id", ou null en cas d'erreur
     */
    ChienDo createChienDo(final ChienDo chienDo);

    /**
     * Permet de rechercher un chien selon son Id
     * 
     * @param idchienDo l'id du chien à rechercher
     * @return le produit recherché, null sinon
     */
    ChienDo findChienDo(final Integer idChienDo);

    /**
     * Permet de retrouver tous les chiens
     * 
     * @return la liste de tous les chiens, vide ou remplie
     */
    List<ChienDo> findAllChienDo();

    /**
     * Permet de mettre à jour un chien
     * 
     * @param chienDo le chien
     * @return le chien mis à jour, null en cas de problème
     */
    ChienDo updateChienDo(final ChienDo chienDo);

    /**
     * Permet de supprimer un chien selon son id
     * 
     * @param idChienDo l'id du produit à supprimer
     * @return true si OK, false sinon
     */
    boolean deleteChienDo(final Integer idChienDo);

}
