package persistence.dao;

import java.util.List;

import persistence.bean.Client;

public interface IClientDao {
	
	/**
	 * Permet de créer un client en BD
	 * 
	 * @param client le client à créer en BD
	 * @return le client créé, avec son "Id", ou null en cas d'erreur
	 */
	Client findByLoginAndPassword(final String login,final String password);

	/**
	 * Permet de créer un client en BD
	 * 
	 * @param client le client à créer en BD
	 * @return le client créé, avec son "Id", ou null en cas d'erreur
	 */
	Client create(final Client client);

	/**
	 * Permet de rechercher un client selon son Id
	 * 
	 * @param idclient l'id du client à rechercher
	 * @return le produit recherché, null sinon
	 */
	Client findById(final Integer idClient);

	/**
	 * Permet de retrouver tous les clients
	 * 
	 * @return la liste de tous les clients, vide ou remplie
	 */
	List<Client> findAll();

	/**
	 * Permet de mettre à jour un client
	 * 
	 * @param client le client
	 * @return le client mis à jour, null en cas de problème
	 */
	Client update(final Client client);

	/**
	 * Permet de supprimer un client selon son id
	 * 
	 * @param idClient l'id du produit à supprimer
	 * @return true si OK, false sinon
	 */
	void deleteById(final Integer idClient);

	Boolean findByEmail(String login);


}
