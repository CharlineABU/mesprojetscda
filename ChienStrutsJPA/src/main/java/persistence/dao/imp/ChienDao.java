/**
 * 
 */
package persistence.dao.imp;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import persistence.bean.ChienDo;
import persistence.dao.IChienDao;
import persistence.factory.JpaFactory;

/**
 * @author Charline
 *
 */
public class ChienDao implements IChienDao {

    private static ChienDao chienDao = null;

    /**
     * Constructeur
     */
    public ChienDao() {
        super();
    }

    /**
     * Gestion du singleton
     * 
     * @return chienDao
     */
    public static ChienDao getInstance() {
        if (chienDao == null) {
            chienDao = new ChienDao();
        }
        return chienDao;
    }

    @Override
    public ChienDo createChienDo(final ChienDo chienDo) {
        //je recupère l'entityManagerFactory
        final EntityManagerFactory entityManagerFactory = JpaFactory.getEntityManagerFactory();
        //je crée l'entityManager
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        //je crée l'entityTransaction 
        final EntityTransaction entityTransaction = entityManager.getTransaction();
        //je debute la transaction
        entityTransaction.begin();

        //je rattache mon chienDo à l'entityManager (provocation de la sauvegarde:Non-managé à managé)
        entityManager.persist(chienDo);

        // je termine l'entityTransaction ==> synchronisation avec la BD, génération des instructions
        entityTransaction.commit();
        //je ferme l'entityManager
        entityManager.close();

        //je retourne le produitDo crée
        return chienDo;
    }

    @Override
    public ChienDo findChienDo(final Integer idChienDo) {
        //je recupère l'entityManagerFactory
        final EntityManagerFactory entityManagerFactory = JpaFactory.getEntityManagerFactory();
        //je crée l'entityManager
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        //je crée l'entityTransaction 
        final EntityTransaction entityTransaction = entityManager.getTransaction();
        //je debute la transaction
        entityTransaction.begin();

        //je recupère le chien dans la BDD 
        final ChienDo chienDo = entityManager.find(ChienDo.class, idChienDo);

        // je termine l'entityTransaction ==> synchronisation avec la BD, génération des instructions
        entityTransaction.commit();

        //je ferme l'entityManager
        entityManager.close();

        //je retourne le chienDo trouvé
        return chienDo;
    }

    @Override
    public List<ChienDo> findAllChienDo() {
        //je recupère l'entityManagerFactory
        final EntityManagerFactory entityManagerFactory = JpaFactory.getEntityManagerFactory();
        //je crée l'entityManager
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        //je crée l'entityTransaction 
        final EntityTransaction entityTransaction = entityManager.getTransaction();
        //je debute la transaction
        entityTransaction.begin();

        //avec NamedQuery
        //je cree ma requete jpql
        final String jpql = "all";

        //je crée ma typedQuery
        final TypedQuery<ChienDo> query = entityManager.createNamedQuery(jpql, ChienDo.class);

        //je Stocke le resultat de l'execution de la query dans une liste
        final List<ChienDo> listeDesChienDo = query.getResultList();

        // je termine l'entityTransaction ==> synchronisation avec la BD, génération des instructions
        entityTransaction.commit();

        //je ferme l'entityManager
        entityManager.close();

        //je retourne la liste d'entité
        return listeDesChienDo;
    }

    @Override
    public ChienDo updateChienDo(final ChienDo chienDo) {
        //je recupère l'entityManagerFactory
        final EntityManagerFactory entityManagerFactory = JpaFactory.getEntityManagerFactory();
        //je crée l'entityManager
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        //je crée l'entityTransaction 
        final EntityTransaction entityTransaction = entityManager.getTransaction();
        //je debute la transaction
        entityTransaction.begin();

        // on update l'objet
        entityManager.merge(chienDo);

        // je termine l'entityTransaction ==> synchronisation avec la BD, génération des instructions
        entityTransaction.commit();
        //je ferme l'entityManager
        entityManager.close();

        //je retourne le chienDo màj
        return chienDo;
    }

    @Override
    public boolean deleteChienDo(final Integer idChienDo) {
        //je recupère l'entityManagerFactory
        final EntityManagerFactory entityManagerFactory = JpaFactory.getEntityManagerFactory();
        //je crée l'entityManager
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        //je crée l'entityTransaction 
        final EntityTransaction entityTransaction = entityManager.getTransaction();
        //je debute la transaction
        entityTransaction.begin();

        //on catch l'exception au cas où aucun produit n'est rattaché
        try {
            //on rattache l'objet à l'entityManager
            entityManager.remove(entityManager.getReference(ChienDo.class, idChienDo));
        } catch (final EntityNotFoundException entityNotFoundException) {
            entityNotFoundException.printStackTrace();
            // je termine l'entityTransaction ==> synchronisation avec la BD, génération des instructions
            entityTransaction.rollback();
            //je ferme l'entityManager
            entityManager.close();
            return false;
        }

        // je termine l'entityTransaction ==> synchronisation avec la BD, génération des instructions
        entityTransaction.commit();
        //je ferme l'entityManager
        entityManager.close();

        //je retourne true si delete OK
        return true;
    }

}
