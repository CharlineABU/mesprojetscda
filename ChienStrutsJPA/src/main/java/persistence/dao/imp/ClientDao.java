/**
 * 
 */
package persistence.dao.imp;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import persistence.bean.Client;
import persistence.dao.IClientDao;

/**
 * @author Charline
 *
 */
@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class ClientDao implements IClientDao {

	// Pour montrer les 2 types d'injection
	@PersistenceContext(unitName = "puChien", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	@Override
	public Client findByLoginAndPassword(String login, String password) {
		TypedQuery<Client> typedQuery = entityManager.createNamedQuery("rechercheParLoginPwd", Client.class);
		typedQuery.setParameter("email", login);
		typedQuery.setParameter("password", password);
		Client client = null;
		try {
			client = typedQuery.getSingleResult();
		} catch (NoResultException e) {
			System.err.println("pas de resultats");
		}

		return client;
	}

	@Override
	public Client create(Client client) {
		entityManager.persist(client);
		return client;
	}

	@Override
	public Client findById(Integer idClient) {
		return entityManager.find(Client.class, idClient);
	}

	@Override
	public List<Client> findAll() {
		TypedQuery<Client> typedQuery = entityManager.createQuery("select c from Client c", Client.class);
		return typedQuery.getResultList();
	}

	@Override
	public Client update(Client client) {
		return entityManager.merge(client);
	}

	@Override
	public void deleteById(Integer idClient) {
		Client c = new Client();
		c.setId(idClient);

		entityManager.remove(c);
	}

	@Override
	public Boolean findByEmail(String login) {
		TypedQuery<String> typedQuery = entityManager.createQuery("select 'true'  from Client c WHERE c.email=:email",
				String.class);
		typedQuery.setParameter("email", login);

		try {
			typedQuery.getSingleResult();
			return true;
		} catch (NoResultException e) {
			System.err.println("pas de resultats");
		}
		return false;
	}

}
