/**
 * 
 */
package persistence.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Charline
 *
 */
@Data
@Entity(name = "ChienDo")
@Table(name = "chien")
@NamedQuery(name = "all", query = "SELECT c From ChienDo c")
public class ChienDo {

    @Id
    @Column(name = "id_chien")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "num_puce")
    private Integer num_puce;

    @Column(name = "nom")
    private String  nom;

    @Column(name = "couleur")
    private String  couleur;

    @Column(name = "age_chien")
    private Integer age_chien;
    
    @Column(name = "lien_image")
    private String lien_image;
    
    

    /**
     * Constructeur
     */
    public ChienDo() {
        super();
    }

    /**
     * Permet de construire un ChienDo
     * 
     * @param num_puce
     * @param nom
     * @param couleur
     * @param age_chien
     * @param image
     * @return
     */
    public ChienDo builder(final Integer num_puce, final String nom, final String couleur, final Integer age_chien, final String lien_image) {
        final ChienDo chienDo = new ChienDo();
        chienDo.setNum_puce(num_puce);
        chienDo.setNom(nom);
        chienDo.setCouleur(couleur);
        chienDo.setAge_chien(age_chien);
        chienDo.setLien_image(lien_image);

        return chienDo;
    }

}
