/**
 * 
 */
package presentation.bean;

import lombok.Data;

/**
 * @author pc
 *
 */
@Data
public class ChienDto {

    private Integer id;
    private Integer num_puce;
    private String  nom;
    private String  couleur;
    private Integer age_chien;
    private String  lien_image;

    /**
     * Constructeur
     */
    public ChienDto() {
        super();
    }

}
