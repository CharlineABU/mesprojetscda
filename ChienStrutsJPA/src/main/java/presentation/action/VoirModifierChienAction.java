/**
 * 
 */
package presentation.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import presentation.bean.ChienDto;
import service.imp.ChienService;

/**
 * @author pc
 *
 */
public class VoirModifierChienAction extends Action {

    /**
     * 
     */
    public VoirModifierChienAction() {
       super();
    }
 
    
    @Override
    public ActionForward execute(final ActionMapping mapping,final ActionForm form, final HttpServletRequest request,final HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        // recupere l'id du chien à modifier
        final Integer id = Integer.valueOf(request.getParameter("id"));
        //recupere le chien dans la BDD
        final ChienService chienService = ChienService.getIntance();
        ChienDto chienDto=chienService.findChienDtoById(id);
        //mettre le chien dans la requete
        session.setAttribute("chien", chienDto);
        return mapping.findForward("success");
    }

}
