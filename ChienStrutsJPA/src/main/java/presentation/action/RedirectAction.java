/**
 * 
 */
package presentation.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author pc
 *
 */
public class RedirectAction extends Action {

    /**
     * Constructeur
     */
    public RedirectAction() {
        super();
    }

    
    @Override
    public ActionForward execute(final ActionMapping mapping,final  ActionForm form, final HttpServletRequest request,final  HttpServletResponse response) throws Exception {
        
        //redirection vers une jsp
        
        return mapping.findForward("success");
    }
}
