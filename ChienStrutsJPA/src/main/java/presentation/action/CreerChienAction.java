/**
 * 
 */
package presentation.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import presentation.bean.ChienDto;
import presentation.form.ChienForm;
import service.imp.ChienService;

/**
 * @author pc
 *
 */
public class CreerChienAction extends Action {

    /**
     * 
     */
    public CreerChienAction() {
       super();
    }
    
    /**
     * Permet de mapper un Chienform en chienDto
     * 
     * @param chienForm
     * @return chienDto
     */
    public static ChienDto mapChienFormToChienDto(final ChienForm chienForm) {
        final ChienDto chienDto = new ChienDto();
        chienDto.setNum_puce(Integer.valueOf(chienForm.getNum_puce()));
        chienDto.setNom(chienForm.getNom());
        chienDto.setCouleur(chienForm.getCouleur());
        chienDto.setAge_chien(Integer.valueOf(chienForm.getAge_chien()));
        chienDto.setLien_image(chienForm.getLien_image());
        
        return chienDto;
        
    }
    
    @Override
    public ActionForward execute(final ActionMapping mapping,final ActionForm form, final HttpServletRequest request,final HttpServletResponse response) throws Exception {
        
        // recupere le formulaire chien
        final ChienForm chienForm = (ChienForm) form;
        // recupere les info du chienform et on creer le chien en BDD
        final ChienService chienService = ChienService.getIntance();
        final ChienDto newChienDto = chienService.createChienDto(mapChienFormToChienDto(chienForm));
        // afficher le chien ajouter avec message d'ajout OK/KO
        if (newChienDto == null) {
            final ActionErrors errors = new ActionErrors();
            errors.add("error", new ActionMessage("errors.creation", new Object[] { chienForm.getNum_puce() }));
            saveErrors(request, errors);
        } else {
            final ActionMessages messages = new ActionMessages();
            messages.add("creationOK", new ActionMessage("creer.ok", new Object[] { chienForm.getNum_puce() }));
            saveMessages(request, messages);
        }
        
        return mapping.findForward("success");
    }

}
