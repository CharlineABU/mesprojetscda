/**
 * 
 */
package presentation.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import presentation.bean.ChienDto;
import service.imp.ChienService;

/**
 * @author Charline
 *
 */
public class ListerChienAction extends Action {

    /**
     * Constructeur
     */
    public ListerChienAction() {
        super();
    }

    @Override
    public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
        //recupérer la liste 
        final ChienService chienService = new ChienService();
        final List<ChienDto> listeChiens = chienService.findAllChienDto();
        //mettre la liste récupere dans la session
        request.setAttribute("listeChiens", listeChiens);

        return mapping.findForward("success");
    }

}
