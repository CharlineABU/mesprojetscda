/**
 * 
 */
package presentation.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import presentation.bean.ChienDto;
import presentation.form.ChienForm;
import service.imp.ChienService;

/**
 * @author pc
 *
 */
public class ModifierChienAction extends Action {

    /**
     * Constructeur
     */
    public ModifierChienAction() {
        super();
    }

    /**
     * Permet de mapper un Chienform en chienDto
     * 
     * @param chienForm
     * @return chienDto
     */
    public static ChienDto mapChienFormToChienDto(final ChienForm chienForm) {
        final ChienDto chienDto = new ChienDto();
        chienDto.setId(Integer.valueOf(chienForm.getId()));
        chienDto.setNum_puce(Integer.valueOf(chienForm.getNum_puce()));
        chienDto.setNom(chienForm.getNom());
        chienDto.setCouleur(chienForm.getCouleur());
        chienDto.setAge_chien(Integer.valueOf(chienForm.getAge_chien()));
        chienDto.setLien_image(chienForm.getLien_image());

        return chienDto;

    }

    @Override
    public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        //recuperer le formulaire chien
        final ChienForm chienForm = (ChienForm) form;
        //chercher le chien en BDD
        final ChienService chienService = ChienService.getIntance();
        final ChienDto newChienDto = chienService.updateChienDto(mapChienFormToChienDto(chienForm));
        // on teste le retour du service
        if (newChienDto == null) {
            final ActionErrors errors = new ActionErrors();
            errors.add("updateKO", new ActionMessage("errors.modification", new Object[] { chienForm.getNum_puce() }));
            saveErrors(request, errors);
        } else {
            final ActionMessages messages = new ActionMessages();
            messages.add("updateOK", new ActionMessage("editer.ok", new Object[] { chienForm.getNum_puce() }));
            saveMessages(request, messages);
            session.setAttribute("chien", newChienDto);
        }
        return mapping.findForward("success");
    }

}
