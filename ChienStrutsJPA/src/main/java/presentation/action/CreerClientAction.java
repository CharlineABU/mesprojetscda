/**
 * 
 */
package presentation.action;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import presentation.form.ClientForm;
import service.IClientService;
import service.dto.ClientDto;

/**
 * @author pc
 *
 */
public class CreerClientAction extends Action {
	// recuperer l'application context à partir du serveur (configurer dans le
	// web.xml)
//			WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(this.getServlet().getServletContext());
//			clientService = context.getBean(IClientService.class);
	// ou bien
	// recuperer l'application context pour application java ou les test JUNIT
//			 ApplicationContext contextForJavaOrTestJunit=
//			 ContextFactory.getContext(ContextConfigurationType.CLASSPATH);
//			 chienService = contextForJavaOrTestJunit.getBean(IChienService.class);
	@Autowired
	IClientService clientService;
	@Autowired
	DateFormat dateFormat;

	/**
	 * Permet de mapper un Chienform en chienDto
	 * 
	 * @param chienForm
	 * @return chienDto
	 * @throws ParseException
	 */
	public ClientDto mapClientFormToClientDto(final ClientForm clientForm) throws ParseException {

		final ClientDto clientDto = new ClientDto();
		clientDto.setNom(clientForm.getNom());
		clientDto.setPrenom(clientForm.getPrenom());
		clientDto.setDateNaissance(dateFormat.parse(clientForm.getDate_naissance()));
		clientDto.setEmail(clientForm.getEmail());
		clientDto.setPassword(clientForm.getPassword());

		return clientDto;

	}

	@Override
	public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {
		// recupere le formulaire chien
		final ClientForm clientForm = (ClientForm) form;

		ClientDto clientDto = mapClientFormToClientDto(clientForm);

		// recupere les info du chienform et on creer le chien en BDD
		final ClientDto newClientDto = clientService.createClientDto(clientDto);
		// afficher le chien ajouter avec message d'ajout OK/KO
		if (newClientDto == null) {
			final ActionErrors errors = new ActionErrors();
			errors.add("error", new ActionMessage("errors.creation", new Object[] { newClientDto.getNom() }));
			saveErrors(request, errors);
		} else {
			final ActionMessages messages = new ActionMessages();
			messages.add("creationOK", new ActionMessage("creer.ok", new Object[] { newClientDto.getNom() }));
			saveMessages(request, messages);
		}

		return mapping.findForward("success");
	}

}
