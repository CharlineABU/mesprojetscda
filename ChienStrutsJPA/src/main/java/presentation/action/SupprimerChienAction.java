/**
 * 
 */
package presentation.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import service.imp.ChienService;

/**
 * @author pc
 *
 */
public class SupprimerChienAction extends Action {

    /**
     * Constructeur
     */
    public SupprimerChienAction() {
        super();
    }

    @Override
    public ActionForward execute(final ActionMapping mapping,final  ActionForm form,final HttpServletRequest request,final HttpServletResponse response) throws Exception {

        // recuperer l'id du chien à modifier
        final Integer id = Integer.valueOf(request.getParameter("id"));
        //recuperer le chien dans la BDD
        final ChienService chienService = ChienService.getIntance();
        //supprimer le chien de la BDD
        boolean resultat = chienService.delete(id);
        if (resultat) {
            final ActionMessages messages = new ActionMessages();
            messages.add("deleteOK", new ActionMessage("supprimer.ok"));
            saveMessages(request.getSession(), messages);

        } else {
            final ActionErrors errors = new ActionErrors();
            errors.add("deleteKO", new ActionMessage("errors.suppression"));
            saveErrors(request, errors);

        }

        return mapping.findForward("success");
    }

}
