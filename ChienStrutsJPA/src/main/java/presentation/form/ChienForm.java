/**
 * 
 */
package presentation.form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 * @author pc
 *
 */
public class ChienForm extends ActionForm {

    private String            id;
    private String            num_puce;
    private String            nom;
    private String            couleur;
    private String            age_chien;
    private String            lien_image;

    /**
     * 
     */
    private static final long serialVersionUID = 6438457049925098025L;

    /**
     * Construteur
     */
    public ChienForm() {
        super();
    }
    
    /**
     * Permet de savoir si un mot est composé de chiffres
     * 
     * @param mot
     * @return true si le mot ne contient que des chiffres, false sinon
     */
    private boolean isInteger(final String mot) {
        // ça c'est pour utiliser les Regex, c'est super fort
        return mot.matches("\\d+");
    }

    @Override
    public void reset(final ActionMapping mapping, final HttpServletRequest request) {
        // TODO Auto-generated method stub
        super.reset(mapping, request);
    }

    @Override
    public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();

        //num_puce
        if (num_puce.isEmpty()) {
            errors.add("num_puce", new ActionMessage("errors.num_puce.obligatoire"));
        }else {
            if (!isInteger(num_puce)) {
                errors.add("num_puce", new ActionMessage("errors.num_puce.mumerique"));
            }
        }

        //Nom
        if (nom.isEmpty()) {
            errors.add("nom", new ActionMessage("errors.nom.obligatoire"));
        }

        //couleur
        if (couleur.isEmpty()) {
            errors.add("couleur", new ActionMessage("errors.couleur.obligatoire"));
        }

        //age_chien
        if (age_chien.isEmpty()) {
            errors.add("age_chien", new ActionMessage("errors.age_chien.obligatoire"));
        }else {
            if (!isInteger(age_chien)) {
                errors.add("age_chien", new ActionMessage("errors.age_chien.mumerique"));  
            }
        }

        //lien_image
        if (lien_image.isEmpty()) {
            errors.add("lien_image", new ActionMessage("errors.lien_image.obligatoire"));
        }

        return errors;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getNum_puce() {
        return num_puce;
    }

    public void setNum_puce(final String num_puce) {
        this.num_puce = num_puce;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(final String nom) {
        this.nom = nom;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(final String couleur) {
        this.couleur = couleur;
    }

    public String getAge_chien() {
        return age_chien;
    }

    public void setAge_chien(final String age_chien) {
        this.age_chien = age_chien;
    }

    public String getLien_image() {
        return lien_image;
    }

    public void setLien_image(final String lien_image) {
        this.lien_image = lien_image;
    }

}
