/**
 * 
 */
package presentation.form;

import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import service.IClientService;
import util.ContextConfigurationType;
import util.ContextFactory;

/**
 * @author pc
 *
 */

public class ClientForm extends ActionForm {

	private String nom;
	private String prenom;
	private String email;
	private String password;
	private String date_naissance;

	/**
	 * 
	 */
	private static final long serialVersionUID = 6438457049925098025L;

	/**
	 * Construteur
	 */
	public ClientForm() {
		super();
	}

	@Override
	public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {

		IClientService clientService = ContextFactory.getContext(ContextConfigurationType.CLASSPATH)
				.getBean(IClientService.class);

		ActionErrors errors = new ActionErrors();

		// nom
		if (nom.isEmpty()) {
			errors.add("nom", new ActionMessage("errors.nom.obligatoire"));
		}

		// prenom
		if (prenom.isEmpty()) {
			errors.add("prenom", new ActionMessage("errors.prenom.obligatoire"));
		}

		if (email.isEmpty()) {
			errors.add("email", new ActionMessage("errors.email.obligatoire"));
		}

		if (clientService.ExistsEmail(email)) {
			errors.add("email", new ActionMessage("errors.email.exists"));
		}

		if (password.isEmpty()) {
			errors.add("password", new ActionMessage("errors.password.obligatoire"));
		}

		// date_naissance
		if (date_naissance.isEmpty()) {
			errors.add("date_naissance", new ActionMessage("errors.date_naissance.obligatoire"));

		}

		try {
			new SimpleDateFormat("dd-MM-yyyy").parse(date_naissance);
		} catch (Exception e) {
			errors.add("date_naissance", new ActionMessage("errors.date_naissance.formatInvalide"));
		}

		return errors;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDate_naissance() {
		return date_naissance;
	}

	public void setDate_naissance(String date_naissance) {
		this.date_naissance = date_naissance;
	}

}
