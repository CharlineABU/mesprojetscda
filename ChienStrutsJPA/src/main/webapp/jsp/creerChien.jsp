<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="org.apache.struts.action.ActionErrors"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html>
<html:html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
<title>Enregister Nouveau Chien</title>
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="css/mdb.min.css" rel="stylesheet">
<!-- Your custom styles (optional) -->
<link href="css/style.min.css" rel="stylesheet">
</head>
<body class="grey lighten-3">
	<!-- Navbar -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- Navbar -->

	<!--Main layout-->
	<main class="mt-5 pt-4">
		<div class="container wow fadeIn">

			<!-- Heading -->
			<h2 class="my-5 h2 text-center">Enregistrer Nouveau Chien</h2>

			<!--Grid row-->
			<div class="row" style="width: 1650px; margin: 0 auto;">

				<!--Grid column-->
				<div class="col-md-8 mb-4">

					<!--Card-->
					<div class="card">

						<!--Card content-->
						<html:form method="POST" action="/creer.do" focus="num_puce"
							styleClass="card-body">

							<!--Grid row-->
							<div class="row">

								<!--Grid column-->
								<div class="col-md-6 mb-2">

									<!--Id-->
									<div class="md-form ">
										<html:text property="id" styleClass="form-control"
											readonly="true" />
										<label for="id" class="">Numéro Id</label>
									</div>

								</div>
								<!--Grid column-->

								<!--Grid column-->
								<div class="col-md-6 mb-2">

									<!--numero de puce-->
									<div class="md-form">
										<html:text property="num_puce" styleClass="form-control" />
										<html:errors property="num_puce" header="errors.field.header"
											footer="errors.field.footer" />
										<label for="num_puce" class="">Numéro de la puce</label>
									</div>

								</div>
								<!--Grid column-->

							</div>
							<!--Grid row-->

							<!--nom-->
							<div class="md-form mb-5">
								<html:text property="nom" styleClass="form-control" />
								<html:errors property="nom" header="errors.field.header"
									footer="errors.field.footer" />
								<label for="nom" class="">Nom du chien</label>
							</div>

							<!--couleur-->
							<div class="md-form mb-5">
								<html:text property="couleur" styleClass="form-control" />
								<html:errors property="couleur" header="errors.field.header"
									footer="errors.field.footer" />
								<label for="couleur" class="">Couleur du chien</label>
							</div>

							<!--age_chien-->
							<div class="md-form mb-5">
								<html:text property="age_chien" styleClass="form-control" />
								<html:errors property="age_chien" header="errors.field.header"
									footer="errors.field.footer" />
								<label for="age_chien" class="">Age du chien</label>
							</div>

							<!--lien_image-->
							<div class="md-form mb-5">
								<html:text property="lien_image" styleClass="form-control" />
								<html:errors property="lien_image" header="errors.field.header"
									footer="errors.field.footer" />
								<label for="lien_image" class="">Lien de l'image</label>
							</div>

							<hr class="mb-4">
							<html:submit value="Enregistrer le chien" styleClass="btn btn-primary btn-lg btn-block" />
						</html:form>
						<%-- Permet d'afficher les messages si present --%>
						<logic:messagesPresent message="true">
							<html:messages id="creationOK" property="creationOK" message="true" header="valid.global.header" footer="valid.global.footer">
								<bean:write name="creationOK" />
							</html:messages>
						</logic:messagesPresent>

						<%-- permet d'afficher les erreurs "globales" --%>
						<html:errors header="errors.global.header" property="error" />
						<br>
					</div>
					<!--/.Card-->

				</div>
				<!--Grid column-->

			</div>
			<!--Grid row-->

		</div>
	</main>
	<!--Main layout-->

	<!--Footer-->
	<jsp:include page="footer.jsp"></jsp:include>
	<!--/.Footer-->

	<!-- SCRIPTS -->
	<!-- JQuery -->
	<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
	<!-- Bootstrap tooltips -->
	<script type="text/javascript" src="js/popper.min.js"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<!-- MDB core JavaScript -->
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<!-- Initializations -->
	<script type="text/javascript">
		// Animations initialization
		new WOW().init();
	</script>

</body>
</html:html>