
DELETE FROM chien;

--
-- Contenu de la table chien
--

INSERT INTO chien (id_chien,num_puce, nom, couleur, age_chien,lien_image) VALUES(1,2243, 'Milou', 'Blanc', 2,'img/fox-terrier.jpg');
INSERT INTO chien (id_chien,num_puce, nom, couleur, age_chien,lien_image) VALUES(2,2456, 'Medor', 'Brun mixé', 9,'img/berger-allemand.jpg');
INSERT INTO chien (id_chien,num_puce, nom, couleur, age_chien,lien_image) VALUES(3,2987, 'Roxy', 'Brun/Marron', 5,'img/kelpie.jpg');
INSERT INTO chien (id_chien,num_puce, nom, couleur, age_chien,lien_image) VALUES(4,2754, 'Rex', 'Brun/Marron foncé', 3,'img/berger-belge-tervueren.jpg');
INSERT INTO chien (id_chien,num_puce, nom, couleur, age_chien,lien_image) VALUES(5,2954, 'Billy', 'Sable/Blanc',1,'img/dogue-de-majorque.jpg');
INSERT INTO chien (id_chien,num_puce, nom, couleur, age_chien,lien_image) VALUES(6,2674, 'Luna', 'Blanc/Gris',4,'img/husky-siberien.jpg');
INSERT INTO chien (id_chien,num_puce, nom, couleur, age_chien,lien_image) VALUES(7,2678, 'Rocky', 'Brun/Orange/Blanc',7,'img/chihuahua.jpg');